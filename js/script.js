// let news = document.querySelector(".news"),
//     inpt = document.querySelector("#number"),
//     rqst = new XMLHttpRequest();
//     buttn = document.querySelector("button");
//
// buttn.onclick = event => {
//     inpt.value = "";
//     rqst.open("POST", "http://10.14.206.5/news");
//     rqst.onload("ev.target.response");
//     let data = JSON.parse(ev.target.response),
//         giveNews = {
//             Limit: `${Number(input.value)}`,
//             Page: `${1}`
//         }
//
//     rqst.send(JSON.stringify(giveNews));
// }
const btn = document.querySelector('button');
const newsContainer = document.querySelector('.body');
const input = document.getElementById('number')

btn.addEventListener('click', event => {

    let xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://10.14.206.5/news')

    xhr.onload = event => {
        if (xhr.status != 200) {
            alert(`some error`);

            return
        }

        let data = JSON.parse(event.target.response)

        data.News.forEach(element => {

            let news = document.createElement('div')
            news.classList.add('post')

            news.innerHTML = `
                <img class="post__img" src="http://10.14.206.5/${element.Image}" alt="prewiev">
                <div class="post__title">${element.Caption}</div>
                <p class="post__text">${element.Preview}</p>

                <div class="post__data">${element.Published}</div>   
            `
            newsContainer.appendChild(news)
        });

    }

    let obj = {
        Limit: Number(input.value),
        Page: 1,
    }
    xhr.send(JSON.stringify(obj))

})